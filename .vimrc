set nocompatible                " Disable compatibility with vi which can cause unexpected issues.

filetype on                     " Enable type file detection. Vim will be able to try to detect the type of file in use.

filetype plugin on              " Enable plugins and load plugin for the detected file type.

filetype indent on              " Load an indent file for the detected file type.

syntax on                       " Turn syntax highlighting on.

"set cursorline"                " Highlight cursor line underneath the cursor horizontally. 

"set cursorcolumn"              " Highlight cursor line underneath the cursor vertically.

set number                      " Add numbers to each line on the left-hand side.

set showcmd                     " Show partial command you type in the last line of the screen.

set showmode                    " Show the mode you are on the last line.

set showmatch                   " Show matching words during a search.

set wildmenu                    " Enable auto completion menu after pressing TAB.

set wildmode=list:longest       " Make wildmenu behave like similar to Bash completion.


" PLUGINS ---------------------------------------------------------------- {{{

call plug#begin('~/.vim/plugged')

Plug 'dense-analysis/ale'
Plug 'vim-python/python-syntax'
Plug 'sheerun/vim-polyglot' 
Plug 'jeetsukumaran/vim-pythonsense'

Plug 'vifm/vifm.vim'
Plug 'scrooloose/nerdtree'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight' 
Plug 'ryanoasis/vim-devicons'

Plug 'ap/vim-css-color'
Plug 'frazrepo/vim-rainbow'

Plug 'terryma/vim-multiple-cursors'

call plug#end()

 "}}}


" MAPPINGS --------------------------------------------------------------- {{{

" Mappings code goes here.

 "}}}


" VIMSCRIPT -------------------------------------------------------------- {{{

" This will enable code folding.
" Use the marker method of folding.
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

" More Vimscripts code goes here.

 "}}}


" STATUS LINE ------------------------------------------------------------ {{{

" Status bar code goes here.

 "}}}

 filetype indent on
